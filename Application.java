import java.util.Scanner;

public class Application {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		// Student andy = new Student("Andy Ionita", "2333068");
		// andy.setAverageGrade(100);
		// andy.setId("2333068");
		// System.out.println(andy.getId());
		// andy.setProgram("Gender Studies");
		// andy.setHeight(1.0);
		
		// Student april = new Student("April Katsaros", "2339186");
		// april.setAverageGrade(101);
		// april.setProgram("Computer Science");
		// april.setHeight(1.6256);
		
		// System.out.println("Andy fields");
		
		// System.out.println(andy.getName());
		// System.out.println(andy.getAverageGrade());
		// System.out.println(andy.getId());
		// System.out.println(andy.getProgram());
		// System.out.println(andy.getHeight());
		
		// System.out.println("                 ");
		
		// System.out.println("April fields");
		
		// System.out.println(april.getName());
		// System.out.println(april.getAverageGrade());
		// System.out.println(april.getId());
		// System.out.println(april.getProgram());
		// System.out.println(april.getHeight());
		
		// System.out.println("                 ");
		
		// andy.sleepInClass();
		// andy.logInToComputer();
		
		// System.out.println("                 ");
		
		// april.sleepInClass();
		// april.logInToComputer();
		
		// Student[] section4 = new Student[3];
		// section4[0] = andy;
		// section4[1] = april;
		// section4[2] = new Student("Tin Tran", "6819332");
		
		// section4[2].setAverageGrade(22);
		// section4[2].setProgram("Architecture");
		// section4[2].setHeight(0.9);
		
		// System.out.println("How many times did the last student study?");
		// int amountStudied = Integer.parseInt(sc.nextLine());
		// section4[2].study(amountStudied);
		
		// System.out.println("                 ");
		
		// System.out.println(section4[2].getName());
		// System.out.println(section4[2].getAverageGrade());
		// System.out.println(section4[2].getId());
		// System.out.println(section4[2].getProgram());
		// System.out.println(section4[2].getHeight());
		
		// for(int i = 0; i < section4.length; i++) {
			
			// System.out.println(section4[i].getAmountLearnt());
			
		// }
		
		Student lab = new Student("Tinothy", "4213124");
		
		lab.setHeight(2.1);
		
		System.out.println(lab.getName());
		System.out.println(lab.getAverageGrade());
		System.out.println(lab.getId());
		System.out.println(lab.getProgram());
		System.out.println(lab.getHeight());
		System.out.println(lab.getAmountLearnt());
		
	}
	
}