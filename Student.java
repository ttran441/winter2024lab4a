public class Student {
	
	private String name;
	private int averageGrade;
	private String id;
	private String program;
	private double height;
	private int amountLearnt;
	
	public Student(String name, String id) {
		
		this.name = name;
		this.id = id;
		this.height = 1.0;
		this.amountLearnt = 0;
		
	}
	
	public void sleepInClass() {
		
		System.out.println(this.name + " is sleeping in class!");
		
	}
	
	public void logInToComputer() {
		
		System.out.println(this.name + " of the " + this.program + " program is logging in to the the school computer!" +
		" They input " + this.id + " and their password.");
		
	}

	public void study(int amountStudied) {
		
		amountLearnt += amountStudied;
		
	}
	
	// getters and setters
	public String getName() {
		
		return this.name;
		
	}
	
	public int getAverageGrade() {
		
		return this.averageGrade;
		
	}
	
	public String getId() {
		
		return this.id;
		
	}
	
	public double getHeight() {
		
		return this.height;
		
	}
	
	public String getProgram() {
		
		return this.program;
		
	}
	
	public int getAmountLearnt() {
		
		return this.amountLearnt;
		
	}
	
	// public void setName(String name) {
		
		// this.name = name;
		
	// }
	
	// public void setAverageGrade(int averageGrade) {
		
		// this.averageGrade = averageGrade;
		
	// }
	
	// public void setId(String id) {
		
		// this.id = id;
		
	// }


	public void setHeight(double height) {
		
		this.height = height;
		
	}


	// public void setProgram(String program) {
		
		// this.program = program;
		
	// }


	
}